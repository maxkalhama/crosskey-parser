const PDFParser = require('pdf2json')

/**
 * Wrapping pdfParser inside a promise
 * @param buffer 
 */
export const pdfParser = (buffer: Buffer): Promise<any> => {
  return new Promise((resolve, reject) => {
    const pdfParser = new PDFParser()
    
    pdfParser.on("pdfParser_dataError", (err: any) => {
      reject(err)
    })
    
    pdfParser.on("pdfParser_dataReady", (pdf: any) => {
      resolve(pdf)
    })

    pdfParser.parseBuffer(buffer)
  })
}