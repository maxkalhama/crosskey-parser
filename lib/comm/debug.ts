/**
 * console.debug. Was lazy not editing console so you have to use debug(msg)
 * 
 * @param msg 
 * @param error 
 */
export const debug = (msg: any, error?: boolean) => {
  if (process.env.DEBUG == 'true') {
    const log = `${new Date().toISOString()} ${msg}`
    error ? console.error(log) : console.log(log)
  }
}