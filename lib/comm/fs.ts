import * as fs from 'fs'
import { debug } from './debug'
import { resolve } from 'url';

// WRAPPING FS FUNCTIONS WITH PROMISE

export const writeFile = (filename: string, data: any): Promise<any> => {
  debug('Writing to file')
  return new Promise(function(resolve, reject) {
    fs.writeFile(filename, data, function(err) {
      if (err) reject(err);
      else resolve(data);
    });
  });
}

const readFile = (input: string): Promise<Buffer> => {
  debug('Reading file')
  return new Promise(function(resolve, reject) {
    fs.readFile(input, (err, buffer) => {
      if (err) reject(err)  
      resolve(buffer)
    })
  });
}

const readFileNames = (input: string): Promise<string[]> => {
  return new Promise(function(resolve, reject) {
    fs.readdir(input, (err, files) => {
      if (err) reject(err)
      resolve(files)
    })
  })
}

export const readFiles = async (input: string): Promise<Buffer[]> => {
  debug('Reading file')
  if (!fs.lstatSync(input).isDirectory()) {
    return [await readFile(input)]
  } else {
    const filenames = await readFileNames(input)
    const ret = []
    for (const path of filenames) {
      ret.push(await readFile(`${input}${input.slice(-1) === '/' ? '' : '/'}${path}`))
    }
    return ret
  }
}