import { IPage } from "../interfaces/IPage";
import { Text } from "./Text";
import * as _ from 'lodash'
import { Area } from "./Area";
import { DateArea } from "./DateArea";
import { Row } from "./Row";

export class Page extends Area {
  private columns: number[]
  private rows: number[]
  private i: number

  /**
   * Page represents one pdf page
   * 
   * @param Page json of parsed pdf
   * @param i page number
   */
  constructor(Page: IPage, i: number) {
    // map texts to text class
    const Texts = Page.Texts.map(txt => new Text(txt))
    super(Texts)
    // map columns and rows
    this.columns = Page.VLines
      .map(line => line.x - (line.w / 2))
      .filter((x, i, a) => a.indexOf(x) == i)
      .sort((a, b) => (a - b < 0) ? -1 : 1)

    this.rows = Page.HLines
      .map(line => line.y - (line.w / 2))
      .filter((x, i, a) => a.indexOf(x) == i)
      .sort((a, b) => (a - b < 0) ? -1 : 1)

    this.i = i

    // if page consists summary block, define content to end before it
    const summary = this.findMany('YHTEENVETOTIEDOT')
    if (!_.isEmpty(summary)) {
      this.rows[2] = summary[0].y
    }
  }

  /**
   * Get date areas
   */
  public getDateAreas(): DateArea[] {
    const logdate = this.getContent()
      .findMany('KIRJAUSPÄIVÄ')
    const dateYValues = logdate.map(text => {
      return text.y
    })

    // add content first and end rows here, because dates could be divided into multiple pages
    const dateRows = [this.rows[1], ...dateYValues, this.rows[2]]

    const dateAreas = _.initial(dateRows).map((y1, i) => {
      const y2 = dateRows[i + 1]
      
      const dateArea = new DateArea(this.getTable().crop(y1, y2).getTexts(), this.columns, this.rows)
      return dateArea
    })

    return dateAreas
  }

  /**
   * Get rows of page
   */
  public getRows(): Row[] {
    return _.flatten(this.getDateAreas().map(dateArea => dateArea.getRows()))
  }

  /**
   * Get table which contains all data in pdf
   */
  private getTable(): Area {
    const ret = this.crop(this.rows[0], this.rows[2])
    return ret
  }

  /**
   * Reads table headers
   */
  private getHeader(): Area {
    return this.crop(this.rows[0], this.rows[1])
  }

  /**
   * Get content inside table
   */
  private getContent(): Area {
    return this.crop(this.rows[1], this.rows[2])
  }
}