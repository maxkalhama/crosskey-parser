import { IText } from "../interfaces/IText"

export class Text {
  public x: number
  public y: number
  public sw: number
  public w: number
  public R: string


  /**
   * One class represents ony json from parsed pdf
   * 
   * @param Text text json from parsed pdf
   */
  public constructor(Text: IText) {
    this.x = Text.x
    this.y = Text.y
    this.sw = Text.sw
    this.w = Text.w
    this.R = Text.R[0].T
  }

  /**
   * str.includes()
   * 
   * @param str 
   */
  public includes(str: string): boolean {
    return this.R.includes(str)
  }

  /**
   * Is text inisde given reqion
   * 
   * @param y1 
   * @param y2 
   * @param x1 
   * @param x2 
   */
  public isInside(y1?: number, y2?: number, x1?: number, x2?: number): boolean {
    if (x1 && x2 && y1 && y2) {
      return this.x >= x1 && this.x< x2 && this.y >= y1 && this.y < y2
    } else if (x1 && x2) {
      return this.x >= x1 && this.x < x2
    } else if (y1 && y2) {
      return this.y >= y1 && this.y < y2
    } else {
      return true
    }
  }

  /**
   * Does this text represent archive number
   * # 181002390650105090 => true
   */
  public isArchiveNumber(): boolean {
    return new RegExp('^[0-9]{18}.*$').test(this.R)
  }
}