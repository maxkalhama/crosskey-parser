import { Text } from "./Text";
import * as _ from 'lodash'
import { Area } from "./Area";
import { Cell } from "./Cell";
import * as moment from "moment-timezone"
import { IRow } from "../interfaces/IRow";

export class Row extends Area {
  private columns: number[]
  private cells: Cell[]
  public date?: string
  
  /**
   * Row represents one payment row in pdf
   * 
   * @param Texts Texts in row
   * @param columns columns
   * @param date date (if known)
   */
  constructor(Texts: Text[], columns: number[], date?: string) {
    super(Texts)
    this.columns = columns
    this.date = date

    // create cells
    this.cells = _.initial(this.columns)
      .map((x1, i) => {
        const x2 = this.columns[i + 1]
        return new Cell(this.crop(undefined, undefined, x1, x2).getTexts())
      })
  }

  /**
   * Merges this row with the parameter one.
   * 
   * @param another row to merge with
   */
  public merge(another: Row) {
    // this function expects that columns are the same, so we'll inherit columns from this row
    return new Row([...this.getTexts(), ...another.getTexts()], this.columns, this.date)
  }

  /**
   * Returns unparsed and filtered array representation
   */
  public raw(): (string | string[] | undefined)[] {
    return [...(this.cells.map(cell => cell.getTexts().map(text => text.R))), this.date]
  }

  /**
   * Parses row and returns json object
   */
  public parse(): IRow {
    return {
      id: this.tap(),
      type: this.getType(),
      archiveNumber: this.archiveNumber(),
      paymentDate: this.paymentDate(),
      loggingDate: this.loggingDate(),
      party: this.party(),
      message: this.message(),
      card: this.card(),
      cardArchiveNumber: this.cardArchiveNumber(),
      ownBankNumber: this.ownBankNumber(),
      amount: this.amount(),
    }
  }

  private ownBankNumber = (): string | undefined => this.getType() === 'OWN_ACCOUNT_TRANSFER' ? this.val(0, 1) : undefined
  private archiveNumber = (): string | undefined => this.val(0)
  private paymentDate = (): string => moment.utc(this.val(1), 'DDMM', true).format('DD/MM/YYYY')
  private loggingDate = (): string => moment.utc(this.val(1, 1), 'DDMM', true).format('DD/MM/YYYY')
  private party = (): string | undefined => this.val(2)
  private tap = (): string | undefined => this.val(3)
  private message = (): string => {
    const col = this.metadata()
    let start = 3
    let end = col.length
    if (this.getType() === 'CARD_PAYMENT') end = col.length - 1
    else if (this.getType() === 'TRANSFER') start = 2
    return col.slice(start, end).map(text => text.R).join(', ')
  }
  private card = (): string | undefined => {
    const meta = this.metadata()
    return this.getType() === 'CARD_PAYMENT' ? meta[2].R.split('    ')[0] : undefined
  }
  private cardArchiveNumber = (): string | undefined => {
    const meta = this.metadata()
    return this.getType() === 'CARD_PAYMENT' ? meta[2].R.split('    ')[1] : undefined
  }
  private metadata = (): Text[] => this.cells[2].getTexts()
  private amount = (): string | undefined => {
    const txt = this.val(5)
    if (!txt) return undefined
    const str = txt.replace('.', '').replace(',', '.') // "1.234,56-" => "1234.56-"
    const last = str.substr(-1) // will be either - or +
    const float = parseFloat(str) // "1234.56-" => "1234.56"

    return last === '-' ? (float * -1).toFixed(2) : float.toFixed(2)
  }

  private val(col: number, n: number = 0): string | undefined {
    try {
      return this.cells[col].getTexts()[n].R
    } catch {
      return undefined
    }
  }


  /**
   * Get type
   */
  public getType(): 'CARD_PAYMENT' | 'ENTRY_DATE_BALANCE' | 'ENTRY_DATE' | 'PARTIAL' | 'SALARY' | 'OWN_ACCOUNT_TRANSFER' | 'TRANSFER' | 'CASH_TRANSFER' | 'UNKNOWN' {
    const entry = this.cells[0].findOne('KIRJAUSPÄIVÄ')
    const balance = this.cells[4].findOne('SALDO')

    if (entry && balance) {
      return 'ENTRY_DATE_BALANCE'
    }

    if (entry) {
      return 'ENTRY_DATE'
    }
    
    const tap = this.cells[3].getTexts()

    if (tap.length === 0) {
      return 'PARTIAL'
    }

    const cash = this.cells[2].findOne('PANO/OTTO')
    if (cash) {
      return 'CASH_TRANSFER'
    }

    const archive = this.cells[0].getTexts().filter(text => text.isArchiveNumber())[0]

    const salary = this.cells[2].findOne('PALKKA')

    if (salary) {
      return 'SALARY'
    }

    const ownTransfer = this.cells[2].findOne('OMA TILISIIRTO')

    if (ownTransfer) {
      return 'OWN_ACCOUNT_TRANSFER'
    }

    const transfer = this.cells[2].findOne('TILISIIRTO')

    if (transfer) {
      return 'TRANSFER'
    }

    if (archive && archive.includes(' A')) {
      return 'CARD_PAYMENT'
    }
    
    return 'UNKNOWN'
  }
  
  /**
   * Get cells in row
   */
  public getCells(): Cell[] {
    return this.cells
  }
}

