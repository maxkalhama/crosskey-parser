import * as moment from 'moment-timezone'
import { Text } from "./Text"
import { Area } from "./Area"
import { Row } from "./Row"

export class DateArea extends Area {
  private columns: number[]
  private rows: Row[]
  private date?: string
  
  /**
   * Datearea corresponds area for page for one specified day. NOTE: dateArea can also be partial in which case it is actually part of the previous datearea which contains date value
   * 
   * @param Texts 
   * @param columns 
   * @param rows 
   */
  constructor(Texts: Text[], columns: number[], rows: number[]) {
    super(Texts)
    this.columns = columns

    // find individual payments by first searching by archive numbers (one payment got one payment number)
    let breakpoints = this.getTexts()
      .filter(text => text.isArchiveNumber())
      .map(text => text.y)

    breakpoints = [rows[0], ...breakpoints, rows[rows.length - 1]]

    // find date
    const logdate = this.findOne('KIRJAUSPÄIVÄ')
    this.date = !logdate ? undefined : moment.utc(logdate.R.split(' ')[1], 'DD.MM.YY', true).toISOString()

    // chop by breakpoints
    this.rows = this.chop(breakpoints, true)
      .filter(area => area.getTexts().length > 0) // filter areas which contain zero texts
      .map(area => new Row(area.getTexts(), columns, this.date)) // create new payments

    this.rows = this.rows.filter(row => row.getType() !== 'ENTRY_DATE' && row.getType() !== 'ENTRY_DATE_BALANCE') // filter ENTRY_DATE's
  }

  /**
   * Get rows of DateArea
   */
  public getRows(): Row[] {
    return this.rows
  }
}
