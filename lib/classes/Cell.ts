import { Text } from "./Text";
import * as _ from 'lodash'
import { Area } from "./Area";

export class Cell extends Area {  
  constructor(Texts: Text[]) {
    super(Texts)
  }
}
