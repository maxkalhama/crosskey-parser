import { Page } from "./Page";
import * as _ from 'lodash'
import { Row } from "./Row";
import { debug } from "../comm/debug";

export class Doc {
  private Pages: Page[]
  
  constructor(pdf: any) {
    debug('Pre-parsing')
    // Decode %20 to ' ' etc...
    for (var i = 0; i < pdf.formImage.Pages.length; i++){
      for (var j = 0; j < pdf.formImage.Pages[i].Texts.length; j++){
        pdf.formImage.Pages[i].Texts[j].R[0].T = decodeURIComponent(pdf.formImage.Pages[i].Texts[j].R[0].T);
      }
    }

    // remove jatkuus string which seem to have no actual logic where they appear
    pdf.formImage.Pages = pdf.formImage.Pages.map((page: any) => {
      page.Texts = page.Texts.filter((text: any) => {
        return !text.R[0].T.includes('JATKUU')
      })
      return page
    })

    // map pages
    this.Pages = pdf.formImage.Pages.map((page: any, i: number) => new Page(page, i))
  }

  /**
   * Get rows (payments) of document
   */
  public getRows(): Row[] {
    // get rows
    debug('Getting rows')
    const rows =  _.flatten(this.Pages.map(page => page.getRows()))

    debug('Post-parsing rows')
    for(let i = 0; i < rows.length; i++) {
      const row = rows[i]
      // if row has no date, take from the previous one
      if (!row.date) {
        row.date = rows[i - 1].date
      }

      // if row is partial, merge with previous one
      if (row.getType() === 'PARTIAL') {
        rows[i - 1] = rows[i - 1].merge(row)
      }
    }
    // filter partial ones
    const ret = rows.filter(row => row.getType() !== 'PARTIAL')

    return ret
  }

  /**
   * Get JSON representation of document
   */
  public getJson() {
    const rows = this.getRows()
    debug('Converting into JSON')
    return rows.map(row => {
      return row.parse()
    })
  }
}