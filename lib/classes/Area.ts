import { Text } from "./Text"
import * as _ from 'lodash'

export class Area {
  private Texts: Text[]

  constructor(Texts: Text[]) {
    this.Texts = Texts
  }

  /**
   * Finds all matching Text's inside area
   * @param str 
   */
  public findMany(str: string): Text[] {
    return this.Texts.filter(Text => Text.includes(str))
  }

  /**
   * Finds first matching Text inside area
   * @param str 
   */
  public findOne(str: string): Text | undefined {
    return this.Texts.find(Text => Text.includes(str))
  }

  /**
   * Crop creates new area which includes only Texts of current area inside specified region
   * 
   * @param y1 
   * @param y2 
   * @param x1 
   * @param x2 
   */
  public crop(y1?: number, y2?: number, x1?: number, x2?: number): Area {
    const Texts = this.Texts.filter(Text => Text.isInside(y1, y2, x1, x2))
    return new Area(Texts)
  }

  /**
   * Chops / breaks Area into many areas based on breakpoints. 
   * 
   * @param breakpoints Breakpoints used for breaking
   * @param vertical If we should chop area vertically or horizontally
   */
  public chop(breakpoints: number[], vertical: boolean) {
    return _.initial(breakpoints).map((first, i) => {
      const second = breakpoints[i + 1]
      if (vertical) {
        return this.crop(first, second)
      } else {
        return this.crop(undefined, undefined, first, second)
      }
    })
  }

  /**
   * Get texts inside area
   */
  public getTexts(): Text[] {
    return this.Texts
  }
}