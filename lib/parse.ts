import { debug } from "./comm/debug";
import * as json2csv from 'json2csv'
import { readFiles, writeFile } from './comm/fs'
import { Doc } from './classes/Doc'
import { pdfParser } from './comm/pdfParser'

export const parse = async (input: string, output: string, format: 'json' | 'csv' | 'table') => {
  debug('Reading and parsing the file')
  
  let json: any = []

  const fileBuffers = await readFiles(input)
  for (const fileBuffer of fileBuffers) {
    const pdf = await pdfParser(fileBuffer)
    const page = new Doc(pdf).getJson()
    json = [...json, ...page.reverse()]
  }

  json = json.reverse()
  
  switch (format) {
    case 'json':
      if (output) {
        await writeFile(output, JSON.stringify(json))
      } else {
        console.log(json)
      }
      break;
    case 'csv':
      const csv = json2csv.parse(json)
      if (output) {
        await writeFile(output, csv)
      } else {
        console.log(csv)
      }
      break;
    default:
      if (output) {
        throw new Error('Using table output, file output not supported for table')
      }
      console.table(json)
      break;
  }
}