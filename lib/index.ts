#!/usr/bin/env node

import * as program from 'commander'
const cTable = require('console.table')
import * as _ from 'lodash'
import { debug } from './comm/debug';
import { parse } from './parse'
import { IENV } from './interfaces/IENV'

program
  .version('0.0.1', '-V, --version')

program
  .command('parse')
  .description('crosskey-parser parses PDF-files from Crosskey and transforms them into JSON, XML or prints them in a nice table format')
  .option('-i --input <input>', 'Read input file')
  .option('-o --output [output]', 'Output file, if not specified uses stdout')
  .option('-f --format [format]', 'File format (json, csv, table). If not specified uses table')
  .action((env: IENV) => {
    const { input, output, format } = env
    debug(`Read following parameters ${input} ${output} ${format}`)
    parse(input, output, format)
  })

program.parse(process.argv);