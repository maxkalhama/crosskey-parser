interface ILine {
  x: number,
  y: number,
  w: number,
  l: number
}

export interface IHLine extends ILine {}
export interface IVLine extends ILine {}