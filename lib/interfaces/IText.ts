export interface IText {
  x: number,
  y: number,
  w: number,
  sw: number,
  A: string
  R: [{
    T: string
  }]
}
