import { IPage } from "./IPage";

export interface IPDF {
  formImage: {
    Pages: IPage[]
  }
}