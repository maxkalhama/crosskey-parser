export interface IRow {
  id?: string,
  type?: string,
  archiveNumber?: string,
  paymentDate?: string,
  loggingDate?: string,
  party?: string,
  message?: string,
  date?: string,
  card?: string,
  cardArchiveNumber?: string,
  ownBankNumber?: string,
  amount?: string
}