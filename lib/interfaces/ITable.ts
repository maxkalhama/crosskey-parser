import { IText } from "./IText"

export interface ITable extends Array<Array<Array<IText>>> {}
export interface ITableString extends Array<Array<string>> {}
