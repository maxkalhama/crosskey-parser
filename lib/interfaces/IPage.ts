import { IHLine, IVLine } from "./ILine"
import { IText } from "./IText"

export interface IPage {
  Height: number,
  HLines: IHLine[],
  VLines: IVLine[],
  Texts: IText[]
}

