export interface IENV {
  input: string,
  output: string,
  format: 'json' | 'csv' | 'table'
}