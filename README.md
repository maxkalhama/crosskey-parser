# crosskey-parser

crosskey-parser is a module which reads PDF files from Crosskey (banking provider for S-Pankki and Ålandsbanken) and converts them into wanted format

## Installation

1. [install yarn](https://yarnpkg.com/en/docs/install)
2. Install crosskey-parser `yarn global add crosskey-parser`

## Usage
After installation, type `crosskey-parser --help` or `crosskey-parser parse --help` to see how the program can be used.

## Bugs
If you encounter an error you might want to use debugging mode: `DEBUG=true crosskey-parser parse ...`. You even might create a bug report with anonymised sample pdf and hope for the best. 

## Forking, using the code etc...
The codebase can be translated into standard js module rather easily. Create an issue or a pull request for this if you're interested. 
